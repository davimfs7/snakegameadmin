extends Node2D

onready var http = $HTTPRequest
onready var username = $LineEditUsername
onready var password = $LineEditPassword
onready var confirm = $LineEditConfirm
onready var notification = $LabelNotification

func _on_HTTPRequest_request_completed(result, response_code, headers, body):
	var response_body := JSON.parse(body.get_string_from_ascii())
	#print (response_body.result)
	if response_code != 200:
		notification.text = response_body.result.error.message.capitalize()
	else:
		notification.text = "Registration successful!"
		yield(get_tree().create_timer(2.0), "timeout")
		get_tree().change_scene("res://Login.tscn")


func _on_Button_pressed():
	if password.text != confirm.text or username.text.empty() or password.text.empty():
		notification.text = "Invalid password or username!"
		return
	Firebase.register(username.text, password.text, http)


func _on_ButtonBack_pressed():
	get_tree().change_scene("res://Menu.tscn")
