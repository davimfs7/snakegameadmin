extends Panel

signal edit(item)
signal delete(item)

var id = null
var q
var d
var a1
var a2
var a3
var a4
var a5
var correct
var fr
var addTexture = preload("res://Assets/add.png")

func makeNew():
	id = null
	$LabelQuestion.text = "New Question"
	$ButtonDelete.visible = false
	$ButtonEdit.texture_normal = addTexture
	
func loadDocument(doc):
	id = doc.name
	var fields = doc.fields
	q = fields.Question.stringValue
	d = fields.Duration.integerValue
	a1 = fields.Answer1.stringValue
	a2 = fields.Answer2.stringValue
	a3 = fields.Answer3.stringValue
	a4 = fields.Answer4.stringValue
	a5 = fields.Answer5.stringValue
	correct = fields.Correct.integerValue
	fr = fields.Frequency.integerValue
	
	$LabelQuestion.text = q
		

func _on_ButtonEdit_pressed():
	emit_signal("edit",self)


func _on_ButtonDelete_pressed():
	emit_signal("delete",self)
