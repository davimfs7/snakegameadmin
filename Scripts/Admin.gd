extends Node2D

onready var http = $HTTPRequest
onready var httpA = $HTTPadd

const QuestionItem = preload("res://QuestionItem.tscn")

var itemId = null
var deleting = false

func addItem(doc):
	var item = QuestionItem.instance()
	item.rect_min_size = Vector2(470, 70)
	if doc == null:
		item.makeNew()
	else:
		item.loadDocument(doc)
	item.connect("edit", self, "_on_item_edit")
	item.connect("delete", self, "_on_item_delete")
	
	$Panel/ScrollContainer/list.add_child(item)
	
func _on_item_edit(item):
	itemId = item.id
	$RightSide/CheckButtonA1.pressed = false
	$RightSide/CheckButtonA2.pressed = false
	$RightSide/CheckButtonA3.pressed = false
	$RightSide/CheckButtonA4.pressed = false
	$RightSide/CheckButtonA5.pressed = false
	if(item.id == null):
		$RightSide/TextEdit.text = "New question"
		$RightSide/LineEditDuration.text = "5000"
		$RightSide/LineEditAnswer1.text = ""
		$RightSide/LineEditAnswer2.text = ""
		$RightSide/LineEditAnswer3.text = ""
		$RightSide/LineEditAnswer4.text = ""
		$RightSide/LineEditAnswer5.text = ""
		$RightSide/HSlider.value = 50
		$RightSide/ButtonAction.text = "Create"
	else:
		$RightSide/TextEdit.text = item.q
		$RightSide/LineEditDuration.text = item.d
		$RightSide/LineEditAnswer1.text = item.a1
		$RightSide/LineEditAnswer2.text = item.a2
		$RightSide/LineEditAnswer3.text = item.a3
		$RightSide/LineEditAnswer4.text = item.a4
		$RightSide/LineEditAnswer5.text = item.a5
		$RightSide/ButtonAction.text = "Update"
		match int(item.correct):
			1:
				$RightSide/CheckButtonA1.pressed = true
			2:
				$RightSide/CheckButtonA2.pressed = true
			3:
				$RightSide/CheckButtonA3.pressed = true
			4:
				$RightSide/CheckButtonA4.pressed = true
			5:
				$RightSide/CheckButtonA5.pressed = true
		$RightSide/HSlider.value = int(item.fr)

func _on_item_delete(item):
	itemId = item.id
	deleting  = true
	$RightSide/CheckButtonA1.pressed = false
	$RightSide/CheckButtonA2.pressed = false
	$RightSide/CheckButtonA3.pressed = false
	$RightSide/CheckButtonA4.pressed = false
	$RightSide/CheckButtonA5.pressed = false
	$RightSide/TextEdit.text = "New question"
	$RightSide/LineEditDuration.text = "5000"
	$RightSide/LineEditAnswer1.text = ""
	$RightSide/LineEditAnswer2.text = ""
	$RightSide/LineEditAnswer3.text = ""
	$RightSide/LineEditAnswer4.text = ""
	$RightSide/LineEditAnswer5.text = ""
	$RightSide/HSlider.value = 50
	$RightSide/ButtonAction.text = "Create"
	$Dialog.popUpConfirmation(str("Are you sure you want to delete: \"", item.q,"\""))
	#Firebase.delete_document_by_id(item.id, httpA)

func addQuestion():
	
	var data = {
		"Question": {},
		"Duration": {},
		"Answer1": {},
		"Answer2": {},
		"Answer3": {},
		"Answer4": {},
		"Answer5": {},
		"Correct": {},
		"Frequency": {}
	}
	
	data.Question = { "stringValue" : $RightSide/TextEdit.text}
	data.Duration = { "integerValue" : int($RightSide/LineEditDuration.text)}
	data.Answer1 = { "stringValue" : $RightSide/LineEditAnswer1.text}
	data.Answer2 = { "stringValue" : $RightSide/LineEditAnswer2.text}
	data.Answer3 = { "stringValue" : $RightSide/LineEditAnswer3.text}
	data.Answer4 = { "stringValue" : $RightSide/LineEditAnswer4.text}
	data.Answer5 = { "stringValue" : $RightSide/LineEditAnswer5.text}
	if $RightSide/CheckButtonA1.pressed:
		data.Correct = { "integerValue" : 1}
	elif $RightSide/CheckButtonA2.pressed:
		data.Correct = { "integerValue" : 2}
	elif $RightSide/CheckButtonA3.pressed:
		data.Correct = { "integerValue" : 3}
	elif $RightSide/CheckButtonA4.pressed:
		data.Correct = { "integerValue" : 4}
	elif $RightSide/CheckButtonA5.pressed:
		data.Correct = { "integerValue" : 5}
	else:
		data.Correct = { "integerValue" : 1}
	data.Frequency = { "integerValue" : $RightSide/HSlider.value}
		
	if itemId == null:
		Firebase.save_document("Questions", data, httpA)
	else:
		Firebase.update_document_by_id(itemId, data, httpA)
	
	
func _ready():
	Firebase.get_document("Questions", http)

func _on_HTTPRequest_request_completed(result, response_code, headers, body):
	var result_body := JSON.parse(body.get_string_from_ascii()).result as Dictionary
	#print(result_body.documents)
	match response_code:
		400:
			#notification.text = "Please, enter your information"
			$Dialog.popUpInfo(result)
			return
		404:
			#notification.text = "Please, enter your information"
			$Dialog.popUpInfo(result)
			return
		200:
			for child in $Panel/ScrollContainer/list.get_children():
				child.queue_free()
			
			for i in range(0,result_body.documents.size()):
				var doc = result_body.documents[i]
				addItem(doc)
			addItem(null)
			


func _on_ButtonBack_pressed():
	get_tree().change_scene("res://Menu.tscn")


func _on_CheckButtonA1_toggled():
	$RightSide/CheckButtonA2.pressed = false
	$RightSide/CheckButtonA3.pressed = false
	$RightSide/CheckButtonA4.pressed = false
	$RightSide/CheckButtonA5.pressed = false

func _on_CheckButtonA2_toggled():
	$RightSide/CheckButtonA1.pressed = false
	$RightSide/CheckButtonA3.pressed = false
	$RightSide/CheckButtonA4.pressed = false
	$RightSide/CheckButtonA5.pressed = false

func _on_CheckButtonA3_toggled():
	$RightSide/CheckButtonA1.pressed = false
	$RightSide/CheckButtonA2.pressed = false
	$RightSide/CheckButtonA4.pressed = false
	$RightSide/CheckButtonA5.pressed = false

func _on_CheckButtonA4_toggled():
	$RightSide/CheckButtonA1.pressed = false
	$RightSide/CheckButtonA2.pressed = false
	$RightSide/CheckButtonA3.pressed = false
	$RightSide/CheckButtonA5.pressed = false

func _on_CheckButtonA5_toggled():
	$RightSide/CheckButtonA1.pressed = false
	$RightSide/CheckButtonA2.pressed = false
	$RightSide/CheckButtonA3.pressed = false
	$RightSide/CheckButtonA4.pressed = false


func _on_ButtonAction_pressed():
	addQuestion()


func _on_HTTPadd_request_completed(result, response_code, headers, body):
	var result_body := JSON.parse(body.get_string_from_ascii()).result as Dictionary
	#print(result_body.documents)
	match response_code:
		404:
			#notification.text = "Please, enter your information"
			$Dialog.popUpInfo(result)
			return
		200:
			if deleting:
				deleting = false;
				$Dialog.popUpInfo("Question deleted successfuly!")
			else:
				if itemId == null:
					$Dialog.popUpInfo("Question added successfuly!")
				else:
					$Dialog.popUpInfo("Question updated successfuly!")
			Firebase.get_document("Questions", http)


func _on_Dialog_accept():
	if deleting:
		Firebase.delete_document_by_id(itemId, httpA)
