extends Node2D

onready var http = $HTTPRequest
onready var httpfs = $HTTPFS
onready var username = $LineEditUsername
onready var password = $LineEditPassword
onready var notification = $LabelNotification

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


func _on_Button_pressed():
	if username.text.empty() or password.text.empty():
		notification.text = "Enter username and password!"
		return
	Firebase.login(username.text, password.text, http)
	


func _on_HTTPRequest_request_completed(result, response_code, headers, body):
	var response_body = JSON.parse(body.get_string_from_ascii())
	if response_code != 200:
		notification.text = response_body.result.error.message.capitalize()
	else:
		notification.text = "Login successful"
		yield(get_tree().create_timer(1.0), "timeout")
		get_tree().change_scene("res://Menu.tscn")

var data = {
	"question": {},
	"answers": {}	
}

func _on_Button2_pressed():
	print("Sending...")
	#Firebase.get_document("qts", httpfs)
	data.question = { "stringValue" : "This is a question!!!"}
	data.answers = { "stringValue" : "This are answers???"}
	Firebase.save_document("qts", data, httpfs)


func _on_HTTPFS_request_completed(result, response_code, headers, body):
	var result_body := JSON.parse(body.get_string_from_ascii()).result as Dictionary
	print(result_body)
	match response_code:
		404:
			notification.text = "Please, enter your information"
			return
		200:
			notification.text = "Success!!!!!"
			data = result_body.fields
