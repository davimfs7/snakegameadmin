extends Polygon2D

signal accept()
signal cancel()

func popUpConfirmation(text: String):
	$ButtonCancel.visible = true
	$LabelInfo.text = text
	visible = true
	
func popUpInfo(text: String):
	$ButtonCancel.visible = false
	$LabelInfo.text = text
	visible = true

func _on_ButtonConfirm_pressed():
	emit_signal("accept")
	visible = false


func _on_ButtonCancel_pressed():
	emit_signal("cancel")
	visible = false
